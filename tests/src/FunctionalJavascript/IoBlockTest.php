<?php

namespace Drupal\Tests\io\FunctionalJavascript;

/**
 * Tests Intersection Observer API to lazyload ajaxified blocks.
 *
 * @group io
 */
class IoBlockTest extends IoBlockTestBase {

  /**
   * Test IO Blocks by scrolling down the window.
   */
  public function testIoBlockAutoloadOnScroll() {
    parent::doIoBlockAutoloadOnScroll();
  }

  /**
   * Test IO Blocks by manually clicking the fallback links.
   */
  public function testIoBlockManualOnClicking() {
    parent::doIoBlockManualOnClicking();
  }

}
