<?php

namespace Drupal\Tests\io\FunctionalJavascript;

/**
 * Tests Intersection Observer API lazyload ajaxified Views contents.
 *
 * @group io
 */
class IoPagerTest extends IoPagerTestBase {

  /**
   * Test IO Pager by scrolling down the window.
   */
  public function testIoPagerAutoloadOnScroll() {
    parent::doIoPagerAutoloadOnScroll();
  }

  /**
   * Test IO Pager by manually clicking the fallback link.
   */
  public function testIoPagerManualOnClicking() {
    parent::doIoPagerManualOnClicking();
  }

}
