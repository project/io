/**
 * @file
 * Provides IO Browser utilitiy functions.
 */

(function ($, Drupal, _d) {

  'use strict';

  var _nick = 'ib';
  var _root = '.' + _nick;
  var _onIb = _nick + '--on';
  var _onItem = _nick + '-item-container--on';
  var _onDetails = _nick + '--root-hidden--on';
  var _onMediaRendered = 'media--rendered--on';
  var _idIb = _nick;
  var _idItem = _nick + '-item-container';
  var _idDetails = _nick + '-wrapper-hidden';
  var _idMediaRendered = _nick + '-media-rendered';
  var _selIb = _root + ':not(.' + _onIb + ')';
  var _selItem = _root + ' .item-container:not(.' + _onItem + ')';
  var _selDetails = _root + '--root-hidden:not(.' + _onDetails + ')';
  var _selMediaRendered = _root + ' .media--rendered:not(.' + _onMediaRendered + ')';

  Drupal.ioBrowser = Drupal.ioBrowser || {};

  /**
   * Attaches IO Browser common behavior to HTML element.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.ioBrowser = {
    attach: function (context) {
      var me = Drupal.ioBrowser;

      _d.once(me.ib, _idIb, _selIb, context);
      _d.once(me.itemContainer, _idItem, _selItem, context);
      _d.once(me.ibDetails, _idDetails, _selDetails, context);
      _d.once(me.ibMediaRendered, _idMediaRendered, _selMediaRendered, context);
    },
    detach: function (context, setting, trigger) {
      if (trigger === 'unload') {
        $('.ib .item-container', context).find('.button').off('.ibAction');
        _d.once.removeSafely(_idIb, _selIb, context);
        _d.once.removeSafely(_idItem, _selItem, context);
        _d.once.removeSafely(_idDetails, _selDetails, context);
        _d.once.removeSafely(_idMediaRendered, _selMediaRendered, context);
      }
    }
  };

})(jQuery, Drupal, dBlazy);
