/**
 * @file
 * Provides IO Browser view utilitiy functions.
 */

(function ($, Drupal, _d, _win) {

  'use strict';

  var _nick = 'ib';
  var _idView = _nick + '-view';
  var _cViewIb = 'view--ib';
  var _root = '.' + _nick;
  var _onView = _cViewIb + '--on';
  var _selView = '.' + _cViewIb + ':not(.' + _onView + ')';
  var _idGrid = _nick + '-grid';
  var _onGrid = _idGrid + '--on';
  var _baseGrid = '.grid';
  var _selGrid = _root + ' ' + _baseGrid + ':not(.' + _onGrid + ')';
  var _idMedia = _nick + '-media';
  var _onMedia = _idMedia + '--on';
  var _sMedia = _root + ' .media:not(.' + _onMedia + '):not(.media--switch)';
  var _selPreview = _root + '__preview';
  var _cInfoActive = 'is-ib-info-active';
  var _btnInfo = '.button--info';
  var _visuallyHidden = 'visually-hidden';
  var _selIoDone = '.pager__items--end';
  // var _minHeight = 'minHeight';
  // var _selGridContent = _baseGrid + ' .grid__content';

  Drupal.ioBrowser = Drupal.ioBrowser || {};

  /**
   * IO Browser utility functions.
   *
   * @param {HTMLElement} view
   *   The view HTML element.
   */
  function fnView(view) {
    var $view = $(view);

    /**
     * Build plain thumbnails for complex rendered entity for lits/table view.
     *
     * @param {HTMLElement} media
     *   The media HTML element.
     */
    function fnThumbnail(media) {
      var $media = $(media);
      var thumb = $media.data('thumb');

      if (thumb && !$('.media__thumbnail', media).length) {
        $media.append('<img src="' + thumb + '" alt="' + Drupal.t('Thumbnail') + '" class="media__thumbnail visible-list">');
        $media.addClass('media--list');
      }

      $media.addClass(_onMedia);
    }

    // Add a contextual class that IO Browser is active.
    $view.closest('html').addClass('ib-html');

    // Pass the grid info into .grid__content.
    // Replaces complex rendered entity with plain thumbnails for table view.
    _d.once(fnThumbnail, _idMedia, _sMedia, view);

    // After AJAX pager, add ib__main class to view parent element.
    if ($view.parent('div').length) {
      $view.closest('form').find('> div:not(.ib__header, .ib__footer)').addClass('ib__main');
    }

    /**
     * Reacts on clicking pager end.
     *
     * @param {Event} e
     *   The click event.
     */
    function fnIoDone(e) {
      e.preventDefault();

      $(e.target).closest('.pager').addClass(_visuallyHidden);
    }

    $view.on('click', _selIoDone, fnIoDone);

    $view.addClass(_onView);
  }

  /**
   * Build the grid info extracted from exisiting elements.
   *
   * @param {HTMLElement} grid
   *   The grid HTML element.
   */
  function fnGrid(grid) {
    var $grid = $(grid);
    // var previewHeight = 160;

    if (!$(_selPreview, grid).length) {
      return;
    }

    /*
    _win.setTimeout(function () {
      if ($(_selPreview, grid).length) {
        previewHeight = $grid.height();
        $(_selPreview, grid).css(_minHeight, previewHeight);
      }
    }, 100);
    */

    /**
     * Toggle the grid info.
     *
     * @param {jQuery.Event} e
     *   The event triggered by a `click` event.
     *
     * @return {mixed}
     *   Return false if not this, else void.
     */
    function toggleGridInfo(e) {
      e.preventDefault();
      e.stopPropagation();
      var target = e.target;

      if (target !== this) {
        return false;
      }

      var $activeGrid = $(target).closest(_baseGrid);
      $activeGrid.toggleClass(_cInfoActive);

      /*
      if ($(_selPreview, $activeGrid).length) {
        if ($('.media', $activeGrid).length) {
          $(_selGridContent, $activeGrid).css(_minHeight, previewHeight > 120 ? previewHeight + 12 : 160);
        }
      }

      if (!$activeGrid.hasClass(_cInfoActive)) {
        $(_selGridContent, $activeGrid).css(_minHeight, '');
      }
      */
    }

    // Events.
    var cInfo = 'click.ibGridInfo';
    $grid.on(cInfo).on(cInfo, _btnInfo, toggleGridInfo);
    $grid.addClass(_onGrid);
  }

  /**
   * Attaches IO Browser view behavior to HTML element.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.ioBrowserView = {
    attach: function (context) {
      _d.once(fnView, _idView, _selView, context);
      _d.once(fnGrid, _idGrid, _selGrid, context);
    },
    detach: function (context, setting, trigger) {
      if (trigger === 'unload') {
        _d.once.removeSafely(_idView, _selView, context);
        _d.once.removeSafely(_idGrid, _selGrid, context);
      }
    }
  };

})(jQuery, Drupal, dBlazy, this);
