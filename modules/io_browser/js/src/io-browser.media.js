/**
 * @file
 * Provides IO Browser media switch utilitiy functions.
 *
 * This can be used for both IO Browsers and widgets.
 */

(function ($, Drupal, _d, _win) {

  'use strict';

  var _nick = 'ib';
  var _isNick = 'is-' + _nick;
  var _root = '.' + _nick;
  var _rootForm = '.form--' + _nick;
  var _idMedia = _nick + '-media';
  var _onMedia = _idMedia + '--on';
  var _selPlayer = '.media--player';
  var _element = _root + ' ' + _selPlayer + ':not(.' + _onMedia + ')';
  var _isPlaying = 'is-playing';
  var _isZoom = _isNick + '-zoom';
  var _isZoomed = _isNick + '-zoomed';
  var _cZoom = _nick + '__zoom';
  var _icon = '.media__icon';

  Drupal.ioBrowser = Drupal.ioBrowser || {};

  /**
   * IO Browser media utility functions.
   *
   * @param {HTMLElement} media
   *   The media player HTML element.
   */
  function fnMedia(media) {
    var $media = $(media);
    var $form = $media.closest(_rootForm);
    var $ib = $form.length ? $form : $media.closest(_root);
    var $zoom = $('.' + _cZoom, $ib);
    var $body = $ib.closest('body');
    var $wParent = _win.parent.document;
    // var id = 'ib-target';
    var wpad = Math.round((($(_win).height() / $(_win).width()) * 100), 2) + '%';

    // Only when form is preset, the zoom is not available.
    if (!$zoom.length && $form.length) {
      $form.append('<div class="' + _cZoom + '" />');
      $zoom = $('.' + _cZoom, $form);
    }

    /**
     * Play the media.
     *
     * @param {jQuery.Event} e
     *   The event triggered by a `click` event.
     */
    function play(e) {
      var $btn = $(e.currentTarget);
      var $current = $btn.closest(_selPlayer);

      $body.addClass(_isZoom);
      $ib.addClass(_isZoomed);

      setTimeout(function () {
        if ($zoom.length && !$(_selPlayer, $zoom).length) {
          var $clone = $current.clone(true, true);
          $clone.appendTo($zoom);
          $clone.find('img').remove();
          $clone.css('padding-bottom', wpad);
          $current.find('iframe').remove();
        }

        if ($form.length) {
          $('.ui-dialog:visible', $wParent).addClass('ui-dialog--zoom');
        }
        // else {
        // Drupal.ioBrowser.jump(id);
        // }
      });
    }

    /**
     * Close the media.
     *
     * @param {jQuery.Event} e
     *   The event triggered by a `click` event.
     */
    function stop(e) {
      $('.' + _isPlaying).removeClass(_isPlaying);

      $zoom.empty();
      $body.removeClass(_isZoom);
      $ib.removeClass(_isZoomed);

      if ($form.length) {
        $('.ui-dialog:visible', $wParent).removeClass('ui-dialog--zoom');
      }
    }

    var cPlay = 'click.ibMediaPlay';
    var cStop = 'click.ibMediaClose';
    $media.off(cPlay).on(cPlay, _icon + '--play', play);
    $media.off(cStop).on(cStop, _icon + '--close', stop);
    $media.addClass(_onMedia);
  }

  /**
   * Attaches IO Browser media behavior to HTML element.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.ioBrowserMedia = {
    attach: function (context) {
      _d.once(fnMedia, _idMedia, _element, context);
    },
    detach: function (context, setting, trigger) {
      if (trigger === 'unload') {
        _d.once.removeSafely(_idMedia, _element, context);
      }
    }
  };

})(jQuery, Drupal, dBlazy, this);
