<?php

/**
 * @file
 * Provides views data for io_browser.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function io_browser_views_data_alter(&$data) {
  $data['views']['io_browser_switch'] = [
    'title' => t('IO Browser: View Switcher'),
    'help' => t('The IO Browser grid/list (table-like) view switcher.'),
    'filter' => [
      'id' => 'io_browser_switch',
    ],
  ];
}

/**
 * Implements hook_views_plugins_style_alter().
 */
function io_browser_views_plugins_style_alter(array &$plugins) {
  $plugins['io_browser'] = [
    'id'             => 'io_browser',
    'label'          => t('IO Browser'),
    'description'    => t('Display the results in a IO Browser.'),
    'class'          => 'Drupal\io_browser\Plugin\views\style\IoBrowserViews',
    'display_types'  => ['normal'],
    'help'           => t('Works best with filter IO Browser.'),
    'parent'         => 'parent',
    'plugin_type'    => 'style',
    'register_theme' => FALSE,
    'short_title'    => '',
    'theme'          => 'item_list',
    'title'          => t('IO Browser'),
    'provider'       => 'io_browser',
  ];
}
