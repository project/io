<?php

namespace Drupal\io_browser;

use Drupal\blazy\BlazyDefault;

/**
 * Defines shared plugin default settings for field widget and Views style.
 */
class IoBrowserDefault extends BlazyDefault {

  /**
   * Returns the selection entity display plugin settings.
   */
  public static function baseFieldWidgetDisplaySettings() {
    return [
      'ib_context'         => 'widget',
      'entity_type'        => '',
      'display'            => '',
      'selection_position' => 'bottom',
    ];
  }

  /**
   * Returns the views style plugin settings.
   */
  public static function viewsSettings() {
    return [
      'vanilla' => TRUE,
    ] + parent::imageSettings();
  }

  /**
   * Returns the form mode widget base plugin settings.
   */
  public static function widgetBaseSettings() {
    return [
      'ib_context' => 'widget',
      'field_name' => '',
    ] + parent::gridSettings();
  }

  /**
   * Returns the form mode widget plugin settings.
   */
  public static function widgetFileSettings() {
    return [
      'image_style' => 'io_browser_preview',
      'thumbnail_style' => 'io_browser_thumbnail',
    ] + self::widgetBaseSettings();
  }

  /**
   * Returns the form mode rendered entity widget plugin settings.
   */
  public static function widgetEntitySettings() {
    return [
      'entity_type' => '',
      'view_mode' => 'default',
    ]
    + self::widgetBaseSettings();
  }

  /**
   * Returns the form mode media widget plugin settings.
   */
  public static function widgetMediaSettings() {
    return [
      'image' => '',
    ]
      + self::widgetFileSettings()
      + self::widgetEntitySettings();
  }

  /**
   * Returns the extended plugin settings.
   */
  public static function extendedSettings() {
    return self::widgetMediaSettings()
      + self::baseFieldWidgetDisplaySettings()
      + self::entitySettings()
      + self::viewsSettings();
  }

  /**
   * Returns the widget common buttons.
   */
  public static function widgetButtons() {
    return ['preview_link', 'edit_button', 'remove_button', 'replace_button'];
  }

  /**
   * Returns the supported widgets, excluding entity_browser_entity_reference.
   */
  public static function thirdPartyWidgets() {
    return ['entity_browser_file', 'media_library_widget'];
  }

  /**
   * Returns the theme properties.
   */
  public static function themeProperties() {
    return [
      'attributes' => [],
      'content'    => [],
      'items'      => [],
      'settings'   => [],
    ];
  }

}
