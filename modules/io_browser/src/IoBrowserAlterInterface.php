<?php

namespace Drupal\io_browser;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for a IO Browser hook_alter().
 */
interface IoBrowserAlterInterface {

  /**
   * Implements hook_form_alter().
   */
  public function formAlter(&$form, FormStateInterface &$form_state, $form_id);

}
