<?php

namespace Drupal\io_browser;

/**
 * Interface for a IO Browser.
 */
interface IoBrowserInterface {

  /**
   * Returns the io manager.
   */
  public function ioManager();

  /**
   * Returns the blazy admin service.
   */
  public function admin();

  /**
   * Returns the blazy formatter.
   */
  public function formatter();

  /**
   * Returns the blazy entity.
   */
  public function blazyEntity();

  /**
   * Defines common widget form elements.
   */
  public function buildWigetSettingsForm(array &$form, array $definition);

  /**
   * Implements hook_preprocess_views_view().
   */
  public function preprocessViewsView(array &$variables);

  /**
   * Implements hook_preprocess_blazy().
   */
  public function preprocessBlazy(array &$variables): void;

  /**
   * Overrides image style since preview is not always available.
   *
   * Not called after AJAX.
   */
  public function toBlazy(array $display, array &$sets, $delta = 0, $label = NULL): array;

}
