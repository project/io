<?php

namespace Drupal\io_browser;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for IO Browser widget utilities.
 */
interface IoBrowserWidgetInterface extends IoBrowserAlterInterface {

  /**
   * Implements hook_field_widget_WIDGET_TYPE_form_alter().
   */
  public function fieldWidgetFormAlter(
    array &$element,
    FormStateInterface $form_state,
    $context,
  ): void;

}
