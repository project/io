<?php

namespace Drupal\io_browser;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;

/**
 * Provides common utility methods.
 */
class IoBrowserUtil {

  /**
   * Defines the scope for the form elements.
   */
  public static function scopedFormElements() {
    return [
      'caches'            => FALSE,
      'grid_form'         => TRUE,
      // 'grid_required'     => TRUE,
      'opening_class'     => 'form--io-widget',
      'responsive_image'  => FALSE,
      'image_style_form'  => TRUE,
      'style'             => TRUE,
      'no_preload'        => TRUE,
      'no_loading'        => TRUE,
      'no_ratio'          => TRUE,
      'view_mode'         => 'io_browser',
    ];
  }

  /**
   * Prepare settings.
   */
  public static function thirdPartySettings($plugin, $key = NULL, array $defaults = []) {
    // Only available for media_library_widget:
    $config = $plugin->getThirdPartySettings('io_browser');
    $upstreams = $plugin->getSettings();
    $config = array_merge($config, $upstreams[$key] ?? []);
    $upstreams['plugin_id'] = $plugin->getPluginId();

    $widgetsets = [
      'widget' => $upstreams,
    ];

    unset($config['elevatezoomplus']);
    return array_merge($widgetsets, $config) + $defaults;
  }

  /**
   * Add wrappers around the self-closed input element for styling, or iconing.
   *
   * JS is easier, but hence to avoid FOUC.
   */
  public static function wrapButton(array &$input, $key = '', $access = NULL) {
    $css = str_replace('_button', '', $key);
    $css = str_replace('_', '-', $css);
    $title = str_replace(['_', '-'], ' ', $css);

    $attributes = new Attribute();
    $attributes->setAttribute('title', new TranslatableMarkup('@title', ['@title' => Unicode::ucfirst($title)]));
    $attributes->addClass(['button-wrap', 'button-wrap--' . $css]);

    if (isset($access)) {
      $attributes->addClass([$access ? 'is-btn-visible' : 'is-btn-hidden']);
    }

    $content = '';
    if ($key == 'remove' || $key == 'remove_button') {
      // @todo Use JS, not AJAX, for removal - button--ib data-target="remove".
      $content .= '<span class="button--wrap__mask">&nbsp;</span><span class="button--wrap__confirm">' . new TranslatableMarkup('Confirm') . '</span>';
      $attributes->addClass(['button-wrap--confirm']);
    }

    $input['#prefix'] = '<span' . $attributes . '>' . $content;
    $input['#suffix'] = '</span>';
    $input['#attributes']['class'][] = 'button--' . $css;
  }

  /**
   * Returns a group of buttons.
   */
  public static function buildButtons(array $data) {
    $buttons = [];
    foreach ($data as $key) {
      $text = $key == 'info' ? '?' : '&#43;';
      $title = str_replace(['_', '-'], ' ', $key);

      $attributes = new Attribute();
      $attributes->setAttribute('title', new TranslatableMarkup('@title', ['@title' => Unicode::ucfirst($title)]));
      $attributes->setAttribute('type', 'button');
      $attributes->setAttribute('tabindex', '0');
      $attributes->addClass(['button', 'button--' . $key]);

      $buttons[$key] = [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#attributes' => $attributes,
        '#value' => $text,
      ];
    }

    return [
      '#type' => 'inline_template',
      '#template' => '{{ prefix | raw }}{{ buttons }}{{ suffix | raw }}',
      '#context' => [
        'buttons' => $buttons,
        'prefix' => '<div class="button-group button-wrap button-group--grid">',
        'suffix' => '</div>',
      ],
    ];
  }

}
