<?php

namespace Drupal\io_browser\Plugin\EntityBrowser\FieldWidgetDisplay;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\file\FileInterface;
use Drupal\io_browser\IoBrowserDefault;

/**
 * Displays IO Browser File thumbnail if applicable.
 *
 * The main difference from core EB is it strives to display a thumbnail image
 * before giving up to view mode because mostly dealing with small preview.
 *
 * @EntityBrowserFieldWidgetDisplay(
 *   id = "io_browser_file",
 *   label = @Translation("IO Browser: File"),
 *   description = @Translation("Displays a preview of a file or entity using Blazy, if applicable.")
 * )
 */
class IoBrowserFieldWidgetDisplayFile extends IoBrowserFieldWidgetDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return IoBrowserDefault::widgetFileSettings() + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity) {
    if ($entity instanceof FileInterface) {
      $settings = $this->buildSettings();
      $id = $entity->id();

      // @fixme figure out to get deltas like views row index.
      $this->delta++;
      $delta[$id] = $this->delta;

      $data = [
        '#entity'   => $entity,
        '#settings' => $settings,
        '#delta'    => $delta[$id],
        'fallback'  => $entity->getFilename(),
      ];

      $content = $this->blazyEntity->build($data);

      /** @var \Drupal\file\FileInterface $entity */
      $content['#entity'] = $entity;
      return $content;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityTypeInterface $entity_type) {
    return $entity_type->entityClassImplements(FileInterface::class);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    if ($image_style = $this->formatter->load($this->configuration['image_style'], 'image_style')) {
      $dependencies[$image_style->getConfigDependencyKey()][] = $image_style->getConfigDependencyName();
    }
    return $dependencies;
  }

}
