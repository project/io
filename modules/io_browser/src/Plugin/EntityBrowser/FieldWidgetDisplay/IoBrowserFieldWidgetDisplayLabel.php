<?php

namespace Drupal\io_browser\Plugin\EntityBrowser\FieldWidgetDisplay;

use Drupal\Core\Entity\EntityInterface;

/**
 * Displays a label of the entity.
 *
 * @EntityBrowserFieldWidgetDisplay(
 *   id = "io_browser_label",
 *   label = @Translation("IO Browser: Label"),
 *   description = @Translation("Displays an entity label.")
 * )
 */
class IoBrowserFieldWidgetDisplayLabel extends IoBrowserFieldWidgetDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity) {
    if ($denied = $this->denied($entity)) {
      return $denied;
    }
    return $entity->label();
  }

}
