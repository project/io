<?php

namespace Drupal\io_browser\Plugin\EntityBrowser\FieldWidgetDisplay;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\io_browser\IoBrowserDefault;
use Drupal\media\Entity\Media;

/**
 * Displays IO Browser Media thumbnail.
 *
 * @EntityBrowserFieldWidgetDisplay(
 *   id = "io_browser_media",
 *   label = @Translation("IO Browser: Media"),
 *   description = @Translation("Displays a preview of a Media using Blazy, if applicable.")
 * )
 */
class IoBrowserFieldWidgetDisplayMedia extends IoBrowserFieldWidgetDisplayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return IoBrowserDefault::widgetMediaSettings() + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity) {
    if ($entity instanceof Media) {
      $settings = $this->buildSettings();
      $id = $entity->id();

      // @fixme figure out to get deltas like views row index.
      $this->delta++;
      $delta[$id] = $this->delta;

      $ib_label = [
        '#theme'      => 'container',
        '#attributes' => ['class' => ['ib__label']],
        '#children'   => $entity->label(),
      ];

      $data = [
        '#entity'   => $entity,
        '#delta'    => $delta[$id],
        '#settings' => $settings,
        'fallback'  => $entity->label(),
        'overlay'   => ['ib__label' => $ib_label],
      ];

      $content = $this->blazyEntity->build($data);

      /** @var \Drupal\media\Entity\Media $entity */
      $content['#entity'] = $entity;
      return $content;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityTypeInterface $entity_type) {
    return $entity_type->getClass() == 'Drupal\media\Entity\Media'
      || $entity_type->entityClassImplements('MediaInterface');
  }

}
