<?php

namespace Drupal\io;

use Drupal\Core\Form\FormStateInterface;
use Drupal\block\BlockInterface;

/**
 * Defines re-usable services and functions for Io plugins.
 */
interface IoManagerInterface {

  /**
   * Returns the current user service.
   */
  public function currentUser();

  /**
   * Returns the current route match service.
   */
  public function routeMatch();

  /**
   * Returns the plugin block manager service.
   */
  public function blockManager();

  /**
   * Returns the blazy manager.
   */
  public function blazyManager();

  /**
   * Returns the blazy manager.
   */
  public function loadEntityByUuid($uuid, $entity_type = 'block'): ?object;

  /**
   * Checks if the block is applicable for IO.
   *
   * @see io_block_presave()
   * @see self::blockFormAlter()
   */
  public function isBlockApplicable(BlockInterface $block): bool;

  /**
   * Checks if user has access to view IO block, including its path visibility.
   *
   * @see Drupal\io\Controller\IoBlockController
   */
  public function isAllowedBlock(BlockInterface $block): bool;

  /**
   * Checks if we have a valid view for IO pager.
   */
  public function getIoPager($view): ?object;

  /**
   * Returns IO settings.
   */
  public function getIoSettings($type = 'block'): array;

  /**
   * Implements hook_form_FORM_ID_alter().
   */
  public function blockFormAlter(array &$form, FormStateInterface $form_state, $form_id): void;

  /**
   * Implements hook_blazy_form_element_alter().
   */
  public function blazySettingsFormAlter(array &$form): void;

  /**
   * Implements hook_preprocess_HOOK().
   */
  public function preprocessBlock(array &$variables): void;

  /**
   * Returns the block URL.
   */
  public function getBlockUrl($uuid): object;

  /**
   * Returns TRUE if IO block is disabled.
   */
  public function isIoBlockDisabled(): bool;

  /**
   * Return the fallback text.
   */
  public function getFallbackText(): object;

  /**
   * Implements hook_preprocess_io_pager().
   */
  public function preprocessIoPager(array &$variables): void;

}
