<?php

namespace Drupal\io\EventSubscriber;

use Drupal\io\Ajax\IoAppendCommand;
use Drupal\views\Ajax\ViewAjaxResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Response subscriber to handle AJAX responses.
 */
class IoEventSubscriber implements EventSubscriberInterface {

  /**
   * Renders the ajax commands right before preparing the result.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event, which contains the possible AjaxResponse object.
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();

    // Only alter views ajax responses.
    if ($response instanceof ViewAjaxResponse) {
      $this->onViewAjaxResponse($response);
    }
  }

  /**
   * Modifies AJAX response.
   *
   * @param \Drupal\views\Ajax\ViewAjaxResponse $response
   *   The response object, which contains the commands and strings.
   */
  private function onViewAjaxResponse(ViewAjaxResponse &$response) {
    $view = $response->getView();

    // Only alter commands if the user has selected our pager and it
    // attempting to move beyond page 0.
    if ($view->getPager()->getPluginId() !== 'io'
      || $view->getCurrentPage() === 0
      || $view->getPager()->getCurrentPage() === 0) {
      return;
    }

    $style_plugin = $view->getStyle();
    $commands = &$response->getCommands();
    $dom_id = $view->dom_id;

    foreach ($commands as $delta => &$command) {
      $method = $command['method'] ?? '';
      $selector = $command['selector'] ?? '';

      // Remove 'viewsScrollTop' command, as jumping to top is unnecessary.
      // D10 deprecated viewsScrollTop for scrollTop.
      if (in_array($command['command'], ['scrollTop', 'viewsScrollTop'])) {
        unset($commands[$delta]);
      }
      // The replace should the only one, but just in case, we'll make sure.
      // @todo Fix unreliable various AJAX methods.
      elseif ($command['command'] === 'insert'
        && $method === 'replaceWith'
        && $selector == '.js-view-dom-id-' . $dom_id) {

        // Take the data attribute, which is the content of the view,
        // otherwise discard the insert command for the view, we're
        // replacing it with a IoAppendCommand.
        $content = $commands[$delta]['data'];
        unset($commands[$delta]);

        $settings = [
          'style' => $style_plugin->getPluginId(),
          'view_dom_id' => $dom_id,
        ];

        // Table has no way to put [data-io-pager] into tbody element, pass it.
        if ($style_plugin->getPluginId() == 'table') {
          $settings['contentSelector'] = '.views-table tbody';
        }

        $response->addCommand(new IoAppendCommand($content, array_filter($settings)));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::RESPONSE => [['onResponse']]];
  }

}
